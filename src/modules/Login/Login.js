import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';

export default{
	namespaced: true,
	state:{
		login:false,
		datosUsuario:'',
		idusuariosweb: '',
		caja:null,
	},

	mutations:{
		LOGEADO(state, value){
			state.login = value
		},
		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},
		ID_USUARIO(state, idusuariosweb){
			state.idusuariosweb = idusuariosweb
		},

		SALIR(state){
			state.login = false
			state.datosUsuario = ''
			state.idusuariosweb =  ''
		},

		ADDCAJA ( state, caja ){
			state.caja = caja
		}
	},

	actions:{
		// Valida si el usario existe en la BD
		validarUser({commit}, usuario){
			return new Promise((resolve, reject) => {
			 // console.log (usuario)
			  Vue.http.post('sessions', usuario).then(respuesta=>{
			  	return respuesta.json()
			  }).then(respuestaJson=>{
	         // console.log('respuestaJson',respuestaJson)
					if(respuestaJson == null){
						resolve(false) 

					}else{
						resolve(respuestaJson) 
        	}
      	}, error => {
        	reject(error)
      	})
			})
		},

		GetInfoUser({commit, dispatch}, usuario){
			return new Promise((resolve, reject) => {
			  Vue.http.post('sessions', usuario).then(respuesta=>{
      		commit('DATOS_USUARIO',respuesta.body)
					commit('LOGEADO', true)
					resolve(usuario)
	    	}, error => {
	      	reject(error)
	    	})
			})
		},



		salirLogin({commit}){
			commit('SALIR')
			router.push({ name: 'Login'})
		},

		obtenerCaja ( { commit }, caja){
			commit('ADDCAJA', caja)
		} 
	},

	getters:{
		getLogeado(state){
		  return state.login
		},
		getdatosUsuario(state){
			return state.datosUsuario
		},

		getidUsuariosWeb(state){
			return state.idusuariosweb
		},

		getCaja ( state ) {
			return state.caja
		}

	}
}
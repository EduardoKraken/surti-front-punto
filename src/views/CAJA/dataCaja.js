const dataCaja = [
	{  moneda:   '.10',  cantidad: 0, icono:    'mdi-coin',          type:     1 },
	{  moneda:   '.20',  cantidad: 0, icono:    'mdi-coin',          type:     1 },
	{  moneda:   '.50',  cantidad: 0, icono:    'mdi-coin',          type:     1 },
	{  moneda:   '1',    cantidad: 0, icono:    'mdi-coin',          type:     2 },
	{  moneda:   '2',    cantidad: 0, icono:    'mdi-coin',          type:     2 },
	{  moneda:   '5',    cantidad: 0, icono:    'mdi-coin',          type:     2 },
	{  moneda:   '10',   cantidad: 0, icono:    'mdi-coin',          type:     2 },
	{  moneda:   '20',   cantidad: 0, icono:    'mdi-coin',          type:     2 },
	{  moneda:   '20',   cantidad: 0, icono:    'mdi-cash-multiple', type:     3 },
	{  moneda:   '50',   cantidad: 0, icono:    'mdi-cash-multiple', type:     3 },
	{  moneda:   '100',  cantidad: 0, icono:    'mdi-cash-multiple', type:     3 },
	{  moneda:   '200',  cantidad: 0, icono:    'mdi-cash-multiple', type:     3 },
	{  moneda:   '500',  cantidad: 0, icono:    'mdi-cash-multiple', type:     3 },
	{  moneda:   '1000', cantidad: 0, icono:    'mdi-cash-multiple', type:     3 }
]

export { dataCaja }